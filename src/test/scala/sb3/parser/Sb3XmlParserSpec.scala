package sb3.parser

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import ast.{ List => ASTList, _ }
import sb3.parser.Ast2ProgramElems._
import scala.xml.Utility.trim
import sb3.parser.xml.Sb3XmlParser
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._

@RunWith(classOf[JUnitRunner])
class Sb3XmlParserSpec extends FunSuite with BeforeAndAfter {
  var parser: Sb3XmlParser = null

  before {
    parser = new Sb3XmlParser()
  }

  test("read field") {
    val xml = <field name="NUM">5</field>
    val res = parser.parseField(xml)
    assert(res == Field(Map("name" -> "NUM"), "5"))
    assert(res.toXml == xml)
  }

  test("read shadow") {
    val xml = <shadow type="math_number" id="id1"><field name="NUM">5</field></shadow>
    val res = parser.parseShadow(xml)
    assert(res == Shadow(Map("type" -> "math_number", "id" -> "id1"), List(Field(Map("name" -> "NUM"), "5"))))
    assert(res.toXml() == xml)
  }

  test("read value with arg shadow no block") {
    val xml = <value name="val1"><shadow type="text" id="id1"><field name="NUM">5</field></shadow></value>
    val res = parser.parseValue(xml)
    assert(res.toXml == xml)
  }

  test("read simple block with value arg made of shadow") {
    val xml = <block type="t1" id="ID"><value name="val1"><shadow type="text"><field name="NUM">5</field></shadow></value></block>
    val res = parser.parseBlock(xml)
    assert(trim(res.toXml) == trim(xml))
  }

  test("read block with value arg made of shadow and block") {
    val xml = <block type="stmt1"><value name="val1"><shadow type="text"><field name="NUM">5</field></shadow><block type="expr1"></block></value></block>
    val res = parser.parseBlock(xml)
    assert(trim(res.toXml) == trim(xml))
  }

  test("read block with field arg") {
    val xml = <block type="stmt2"><field name="field1">FIELDVAL</field></block>
    val res = parser.parseBlock(xml)
    assert(trim(res.toXml) == trim(xml))
  }

  test("read block with field and value arg") {
    val xml = <block type="stmt3"><field name="field1">FIELDVAL</field><value name="val1"><shadow type="text"><field name="NUM">5</field></shadow></value></block>
    val res = parser.parseBlock(xml)
    assert(trim(res.toXml) == trim(xml))
  }

  test("Value to Ast node infered") {
    val shad = Shadow(Map("type" -> "math_number"), List(Field(Map("name" -> "NUM"), "5")))
    val valueEl = Value(Map("name" -> "val1"), Some(shad), None)
    val res = valueEl.toAstArg()
  }

  test("OperatorExpr to Ast node no arg") {
    val xml = <block type="expr1"></block>
    val ast = parser.parseBlock(xml).toAst()
    assert(trim(ast.toXml) == trim(xml))
  }

  test("OperatorExpr to Ast node with shadow arg") {
    val xml = <block type="expr2"><value><shadow type="math_number"><field name="TEXT">5</field></shadow></value></block>
    val ast = parser.parseBlock(xml).toAst()
    assert(trim(ast.toXml) == trim(xml))
  }

  test("OperatorExpr to Ast node with shadow and blockExpr arg") {
    val xml = <block type="expr3"><value><shadow type="math_number"><field>5</field></shadow><block type="expr"><value><shadow type="math_number"><field name="NUM">5</field></shadow></value></block></value></block>
    val ast = parser.parseBlock(xml).toAst()
    assert(trim(ast.toXml) == trim(xml))
  }

  test("OperatorExpr against real opcode") {
    val xml = <block type="operator_random" id="XdiQmgu@1U//IZDE}1_W" x="51" y="205"><value name="FROM"><shadow type="math_number" id=",-(^n/J~tUFy(COPMu0`"><field name="NUM">1</field></shadow></value><value name="TO"><shadow type="math_number" id="Q`*?.~B/7G]xbokWZpyb"><field name="NUM">10</field></shadow></value></block>
    val ast = parser.parseBlock(xml).toAst()
    assert(trim(ast.toXml) == trim(xml))
  }

  test("ExprStmt to Ast node") {
    val xml = <block type="stmt4">
                <value>
                  <shadow type="math_number" id="id1"><field name="NUM">5</field></shadow>
                  <block type="expr4-1"></block>
                </value>
              </block>
    val ast = parser.parseBlock(xml).toAst().asInstanceOf[ExprStmt]
    assert(trim(ast.toXml) == trim(xml))
  }

  test("ExprStmt on real complete scratchblock") {
    val xml = <block type="motion_glideto" id="wFWAN^yyN+pc=m|Z4p;5" x="129" y="277"><value name="SECS"><shadow type="math_number" id="YZvrhIFVaHum=vF-I4)^"><field name="NUM">1</field></shadow></value><value name="TO"><shadow type="motion_glideto_menu" id="#3r3)XMkUpp:?Fe)Ii8n"><field name="TO">_mouse_</field></shadow></value></block>
    val ast = parser.parseBlock(xml).toAst().asInstanceOf[ExprStmt]
    assert(trim(ast.toXml) == trim(xml))
  }

  test("ExprStmt on real complete scratchblock2") {
    val xml = <block type="motion_gotoxy" id="O[@D#~;4lkOx_f@bpNM9" x="201" y="183"><value name="X"><shadow type="math_number" id="sZsT;m0h.|-kG#P)-!fR"><field name="NUM">0</field></shadow></value><value name="Y"><shadow type="math_number" id="eK3$^l1g9TvO#-pl2EM}"><field name="NUM">0</field></shadow><block type="operator_random" id="eIA=P4fK+alze%8oS1Qm"><value name="FROM"><shadow type="math_number" id="~sHq`F)fWhq12Gtl`665"><field name="NUM">1</field></shadow></value><value name="TO"><shadow type="math_number" id="F`g9TT:=Byp8j*gh[M(U"><field name="NUM">10</field></shadow><block type="operator_length" id="a;,xn]R-}b@fDENT$=mk"><value name="STRING"><shadow type="text" id="xddgQJRGgz%}Fo;.rSnD"><field name="TEXT">world</field></shadow></value></block></value></block></value></block>
    val ast = parser.parseBlock(xml).toAst().asInstanceOf[ExprStmt]
    assert(trim(ast.toXml) == trim(xml))
  }

  test("Boolean OpExp has block input but no default") {
    val xml = <block type="operator_and" id="Ub*SPjFZgktV0g692HAm" x="17" y="73"><value name="OPERAND1"><block type="operator_gt" id="i^W5IKDh=5Usr.95^o$}"><value name="OPERAND1"><shadow type="text" id="AM_UH]3b:Uz@LlhrCgil"><field name="TEXT">1</field></shadow></value><value name="OPERAND2"><shadow type="text" id="p^;x3A^lk2;SMe?GtKx)"><field name="TEXT">2</field></shadow></value></block></value></block>
    val ast = parser.parseBlock(xml).toAst().asInstanceOf[OperatorExpr]
    assert(trim(ast.toXml) == trim(xml))
  }

  test("seq of blocks via next tag") {
    val xml = <block type="motion_movesteps" id="xv:89`*Z{th~3VKjGj)$" x="235" y="-131"><value name="STEPS"><shadow type="math_number" id="ZNarfC/SH~{t.Dg[b,Yw"><field name="NUM">10</field></shadow></value><next><block type="motion_turnright" id="N3OfrhiPPR?z{=@W@0]("><value name="DEGREES"><shadow type="math_number" id="c!AVyT(|%${w]6/`RtUg"><field name="NUM">15</field></shadow></value></block></next></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toBlockSeq()
    assert(trim(ast.toXml()) == trim(xml))
  }

  test("Loop block") {
    val xml = <block type="control_repeat" id="O,!vVQc**5/2:kZRLM:2" x="65" y="119"><value name="TIMES"><shadow type="math_whole_number" id="_wJGN4EyNl(vBZ`C(RJs"><field name="NUM">10</field></shadow></value></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toBlockSeq()
    assert(trim(ast.toXml()) == trim(xml))
  }

  test("Loop block2 with substack named value element") {
    val xml = <block id="LrCxiD;E+1+:]AmtNTWI" type="control_repeat" x="262.5" y="228.8"><value name="TIMES"><shadow id="z#lG{7jGn2WFjHBO#KPU" type="math_whole_number"><field name="NUM">10</field></shadow></value><value name="SUBSTACK"><block id="iLRQqs!BGz=}dhK853SO" type="motion_movesteps"><value name="STEPS"><shadow id="T].[a7h,f.daThor];^[" type="math_number"><field name="NUM">10</field></shadow></value></block></value></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toAst()
    assert(ast.toBlock() == blk)
  }

  test("seq of blocks with loop block") {
    val xml = <block type="control_repeat" id="Os%^s$whNd^cc@=TyTy/" x="-8" y="-5"><value name="TIMES"><shadow type="math_whole_number" id="VOjlRWJvrdG.@MRru05p"><field name="NUM">10</field></shadow></value><statement name="SUBSTACK"><block type="motion_movesteps" id="CcX[)}CniZ@#o{TYKmg3"><value name="STEPS"><shadow type="math_number" id="[7#PHnerwT#?0kwwjLhF"><field name="NUM">10</field></shadow></value></block></statement><next><block type="motion_turnright" id="W:m9w,F!TLf:/xUc3?a%"><value name="DEGREES"><shadow type="math_number" id="Y-xorLdBys*Y=K40)f?{"><field name="NUM">15</field></shadow></value></block></next></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toBlockSeq()
    assert(trim(ast.toXml()) == trim(xml))
  }

  test("If block") {
    val xml = <block type="motion_movesteps" id="4c80!juYbcM1EG~+P@-l" x="136" y="148"><value name="STEPS"><shadow type="math_number" id="KBu3A!t_F8^631vb):xH"><field name="NUM">10</field></shadow></value><next><block type="control_if_else" id="63jg2crcU%~9wWstJ/RE"><value name="CONDITION"><block type="operator_lt" id="laEN2,G*[jE9W~18=w{_"><value name="OPERAND1"><shadow type="text" id="(R,gaKG3k?ME2+!E^Srf"><field name="TEXT"></field></shadow></value><value name="OPERAND2"><shadow type="text" id="#fbGVl?lZs!n.k=MH9.G"><field name="TEXT"></field></shadow></value></block></value><statement name="SUBSTACK"><block type="motion_turnleft" id="f,j@H3T_Ok35xf9Xje,}"><value name="DEGREES"><shadow type="math_number" id="@7zuJ`4e#b%F~/)oAz@^"><field name="NUM">15</field></shadow></value></block></statement><statement name="SUBSTACK2"><block type="motion_pointindirection" id="QdrA{tb0T10=}e/_f5s|"><value name="DIRECTION"><shadow type="math_angle" id="^%%kSlhQBcz^^320I3({"><field name="NUM">90</field></shadow></value></block></statement><next><block type="motion_gotoxy" id="]5zJ2mr!jsY,M({*GdCE"><value name="X"><shadow type="math_number" id="u,Armep-A+v.q3P+[EYN"><field name="NUM">0</field></shadow></value><value name="Y"><shadow type="math_number" id="Zc}WVv~{R~{gi%5WAt1c"><field name="NUM">0</field></shadow></value></block></next></block></next></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toBlockSeq()
    assert(trim(ast.toXml) == trim(xml))
  }
  
  test("If block with value tage for substack"){
    val xml = <block type="control_if_else" id="B,=T]:I/frx)dhMWyKP[" x="375" y="308"><value name="SUBSTACK"><block type="motion_movesteps" id="6_$0o(@~h{RJ0+9WNs*G"><value name="STEPS"><shadow type="math_number" id="U8gXIJ82~%lyTpwwt6|j"><field name="NUM">10</field></shadow></value></block></value><value name="SUBSTACK2"><block type="motion_turnright" id="fho2:+Uull#Xcv8QL4_E"><value name="DEGREES"><shadow type="math_number" id="4QqjLP$d9^b2^]|fWz@R"><field name="NUM">15</field></shadow></value></block></value></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toBlockSeq()
    println(trim(ast.toXml))
    assert(trim(ast.toXml) == trim(ast.toBlock().toXml()))
  }

  test("target") {
    val xml = <xml xmlns="http://www.w3.org/1999/xhtml"><variables><variable type="" id="pKP_I%dxY*Wu|S6eWmMF">a</variable></variables><block type="motion_movesteps" id="dyP~oY#_yH}}s:|2M%Ue" x="172" y="169"><value name="STEPS"><shadow type="math_number" id="%xk=T7Xv/TI3)4,`_8H0"><field name="NUM">10</field></shadow></value></block><block type="motion_turnright" id="oKy6@S.PiuRMVjm}K,jc" x="415" y="165"><value name="DEGREES"><shadow type="math_number" id="4wr~F~VcIWq3wH2QE.0#"><field name="NUM">15</field></shadow></value></block></xml>
    val spriteEl = parser.parseTarget(xml)
    val ast = spriteEl.toAst()
    assert(trim(ast.toXml) == trim(xml))
  }

  test("program") {
    val xml = <program><stage name="Stage"><xml><variables><variable type="" id="F3-s4xm3Q}/L#Pwma04g-a">a</variable></variables><block id="Jz8wXX]Ya2s{yIolu9V-" type="looks_nextbackdrop" x="117" y="327.8"></block></xml></stage><sprite name="Sprite1"><xml><variables><variable type="" id="D@o^{g(Y:u]Oka+nL*rC-b">b</variable></variables><block id="SU%XerU4/)oh0shdabdP" type="motion_movesteps" x="529.5" y="314.6"><value name="STEPS"><shadow id="dldIG@PaLM}%;t=gZsMX" type="math_number"><field name="NUM">10</field></shadow></value></block></xml></sprite><sprite name="Sprite2"><xml><variables></variables><block id="8UJZuVp|PC+*a]WDx-h/" type="motion_turnright" x="313.5" y="349.8"><value name="DEGREES"><shadow id="px4y}~rf6s}[[YS=SmGc" type="math_number"><field name="NUM">15</field></shadow></value></block></xml></sprite></program>
    val programEl = parser.parseProgram(xml)
    val programAst = programEl.toAst().asInstanceOf[Program]
    assert(programAst.toProgramEl() == programEl)
  }

  test("custom block real") {
    val xml = <block id=";oC~sM3$PpV{i0ERfs8," type="procedures_definition" x="30" y="30"><value name="custom_block"><shadow id="zz[?/=KVt/Q!HR1E%a*8" type="procedures_prototype"><mutation proccode="block name %s %b" argumentids="[&quot;P)xRFz`.=Ro}aMOs^mHY&quot;,&quot;3%Aw,/nh/=x|t@HL)Tj4&quot;]" argumentnames="[&quot;number or text&quot;,&quot;boolean&quot;]" argumentdefaults="[&quot;todo&quot;,&quot;todo&quot;]" warp="false"></mutation><value name="P)xRFz`.=Ro}aMOs^mHY"><shadow id="oW%AijKn^LUTPt)285wo" type="argument_reporter_string_number"><field name="VALUE">number or text</field></shadow></value><value name="3%Aw,/nh/=x|t@HL)Tj4"><shadow id="z@.9iG$6ETy`#)/5oG/U" type="argument_reporter_boolean"><field name="VALUE">boolean</field></shadow></value></shadow></value></block>
    val procdef = parser.parseProcDef(xml)
    assert(procdef == procdef.toAst.toProcDefEl)
    println(procdef.toProcDecl().toProcDefEl())
    //    println(procdef.toProcDecl().toProcDefEl().toXml())

  }

  test("custom block with body") {
    val xml = <block id=";@gt+uIE3qmkO1hXP#qC" type="procedures_definition" x="176.66666666666666" y="130.74074074074073"><value name="custom_block"><shadow id="CcvK%9U^sZnb;]I0kRq7" type="procedures_prototype"><mutation proccode="test %s" argumentids="[&quot;M9]ZYkEtx#K*9LbQUXg8&quot;]" argumentnames="[&quot;num&quot;]" argumentdefaults="[&quot;todo&quot;]" warp="false"></mutation><value name="M9]ZYkEtx#K*9LbQUXg8"><shadow id=",_8[DXagHxPp~Pc`9GXk" type="argument_reporter_string_number"><field name="VALUE">num</field></shadow></value></shadow></value><next><block id="iLGX{^2}];lo._rX-)$t" type="motion_movesteps"><value name="STEPS"><shadow id="RN^ek2kkLD5EWK]?VO%_" type="math_number"><field name="NUM">10</field></shadow></value></block></next></block>
    val procdef = parser.parseProcDef(xml)
    assert(procdef == procdef.toAst.toProcDefEl)
    assert(trim(procdef.toXml) == trim(xml))
  }

  test("target to AST having custom block") {
    val xml = <xml xmlns="http://www.w3.org/1999/xhtml"><variables><variable type="" id="pKP_I%dxY*Wu|S6eWmMF">a</variable></variables><block type="motion_movesteps" id="dyP~oY#_yH}}s:|2M%Ue" x="175" y="173"><value name="STEPS"><shadow type="math_number" id="%xk=T7Xv/TI3)4,`_8H0"><field name="NUM">10</field></shadow></value><next><block type="motion_turnright" id="oKy6@S.PiuRMVjm}K,jc"><value name="DEGREES"><shadow type="math_number" id="4wr~F~VcIWq3wH2QE.0#"><field name="NUM">15</field></shadow></value></block></next></block><block type="procedures_definition" id=";@gt+uIE3qmkO1hXP#qC" x="173" y="339"><statement name="custom_block"><shadow type="procedures_prototype" id="CcvK%9U^sZnb;]I0kRq7"><mutation proccode="test %s" argumentids="[&quot;M9]ZYkEtx#K*9LbQUXg8&quot;]" argumentnames="[&quot;num&quot;]" argumentdefaults="[&quot;todo&quot;]" warp="false"></mutation><value name="M9]ZYkEtx#K*9LbQUXg8"><shadow type="argument_reporter_string_number" id=",_8[DXagHxPp~Pc`9GXk"><field name="VALUE">num</field></shadow></value></shadow></statement><next><block type="motion_movesteps" id="iLGX{^2}];lo._rX-)$t"><value name="STEPS"><shadow type="math_number" id="RN^ek2kkLD5EWK]?VO%_"><field name="NUM">10</field></shadow></value></block></next></block></xml>
    val spriteEl = parser.parseTarget(xml)
    val xml2 = spriteEl.toXml()
    assert(trim(spriteEl.toAst().toSpriteEl().toXml()) == trim(xml2))
  }

  test("stage with custom block") {
    val xml = <program><stage><xml><variables><variable type="" id="Ezr,.z7b@B(^:/Vhy!6D-a">a</variable></variables><block type="procedures_definition" id="[[Y@uk:EG]NzLe1Eey0]" x="211" y="63"><statement name="custom_block"><shadow type="procedures_prototype" id="c.eqDmJ.9qHSTe_z1tR~"><mutation proccode="a %n" argumentids="[&quot;input0&quot;]" argumentnames="[&quot;n1&quot;]" argumentdefaults="[1]" warp="false"></mutation><value name="input0"><shadow type="argument_reporter_string_number" id="2lS[-tsV4n0?$1~D-PY|"><field name="VALUE">n1</field></shadow></value></shadow></statement></block><block type="procedures_call" id="KS8R_-5X~Gh/0HABHR/v" x="478" y="69"><mutation proccode="a %n" argumentids="[&quot;input0&quot;]" warp="null"></mutation><value name="input0"><shadow type="math_number" id="LK3eDi-smfFMsjCg_PcR"><field name="NUM">1</field></shadow></value></block></xml></stage><sprite name="Sprite1"><xml><variables><variable type="" id="_ldMC:2N{:#ogr4dsYFQ-b">b</variable></variables></xml></sprite></program>
    val programEl = parser.parseProgram(xml)
    val xml2 = programEl.toXml()
    assert(trim(programEl.toAst().toProgramEl().toXml()) == trim(xml2))
  }

  test("simple procedure call no arg") {
    val xml = <block id="id1" type="procedures_call"><mutation proccode="a" argumentids="[]" warp="null"></mutation></block>
    val blockEl = parser.parseBlock(xml)

    assert(blockEl.toAst.toBlock() == blockEl)
    println("parse procdef-->", blockEl.toAst().asInstanceOf[ProcAccess].toDevString())
    println("parse procdef xml->", blockEl.toAst.toBlock().toXml)
  }

  test("a script containing a simple procedure call") {
    val xml = <block type="procedures_call" id="*A!DfY+agW@!,cIa3LD0" x="-1" y="194"><mutation proccode="a %n" argumentids="[&quot;input0&quot;]" warp="null"></mutation><value name="input0"><shadow type="math_number" id="hbb|VS|Uz4gg.gWtC6~J"><field name="NUM">1</field></shadow></value><next><block type="motion_movesteps" id="JPC?:9Zx`/H.~-gzW@4r"><value name="STEPS"><shadow type="math_number" id="`:;.1ch}T9wfOOso9=hN"><field name="NUM">10</field></shadow></value></block></next></block>
    val blk = parser.parseBlock(xml)
    assert(blk.toBlockSeq().toBlock() == blk)
    println("parse procdef-->", blk.toAst().asInstanceOf[ProcAccess].print())
    println("parse procdef xml->", blk.toBlockSeq().toBlock().toXml)
  }

  /*
   * xml_1 -> block -> xml_2 == xml_1	//original ordering test block->xml
	 * xml_1 -> block -> ast -> block -> xml_3 //stabilize ordering of inputs based on spec
   * xml_3 -> block -> ast -> block -> xml_4 = xml_3 //after stabilized arg ordering in xml source, all should work
   */

  def testBlockCategory(path: String): Unit = {
    val stream = getClass.getResourceAsStream(path)
    val xmlBlocks = scala.io.Source.fromInputStream(stream).getLines.mkString

    parser.stringToXmlElem(xmlBlocks) match {
      case <xml>{ blockEls @ _* }</xml> => {
        blockEls.filter(blkXml => blkXml.label == "block").map { blkXml =>
          {
            try {
              val blk = parser.parseBlock(blkXml)
              assert(trim(blk.toXml) == trim(blkXml))
              val xml_ordered = blk.toAst().toBlock().toXml()
              val blk_ordered = parser.parseBlock(xml_ordered)
              assert(trim(blk_ordered.toAst().toXml()) == trim(xml_ordered))
            } catch {
              case e: Exception => System.err.println("Failed:", blkXml); println(e);
            }
          }
        }
      }
      case something => println(xmlBlocks)
    }
  }

  test("block category") {
    testBlockCategory("/motion.xml")
    testBlockCategory("/looks.xml")
    testBlockCategory("/sound.xml")
    testBlockCategory("/events.xml")
    testBlockCategory("/control.xml")
    testBlockCategory("/control-with-input.xml")
    testBlockCategory("/sensing.xml")
    testBlockCategory("/operators.xml")
    testBlockCategory("/data.xml")
    testBlockCategory("/music.xml")
  }

  test("whenbroadcastreceived") {
    val xml = <block y="784" x="137" id="GkcCN~Rcm0I;gL%_hnmG" type="event_whenbroadcastreceived" xmlns="http://www.w3.org/1999/xhtml"><field variabletype="broadcast_msg" id="-B2byXcH[j-QFQcNuft," name="BROADCAST_OPTION">message1</field></block>
    val blk = parser.parseBlock(xml)
    assert(trim(blk.toAst().toBlock().toXml()) == trim(xml))
  }

  test("broadcast expr with default msg value") {
    val xml = <block type="event_broadcast" id="d})PkCR/wED-;yL,,J8x" x="61" y="176"><value name="BROADCAST_INPUT"><shadow type="event_broadcast_menu" id="j1GP%6Io%({{$)G#,Qqu"><field name="BROADCAST_OPTION" id="0/Ee[3S_zoc5o^X{x/T1" variabletype="broadcast_msg">message1</field></shadow><block type="data_variable" id="~v#fYV+l%ZVXfMF]T`m5"><field name="VARIABLE" id="d)PWQ/a;K;sJGp+ce/i=" variabletype="">a</field></block></value></block>
    val blk = parser.parseBlock(xml)
    println(blk)
    println(blk.toAst.toBlock)
    assert(blk == blk.toAst().toBlock())
    assert(trim(xml) == trim(blk.toAst().toBlock().toXml()))
  }

  test("broadcast msg") {
    val xml = <block type="event_broadcast" id="F*AD%3an0$e9;*YMVc/t" x="132" y="124"><value name="BROADCAST_INPUT"><shadow type="event_broadcast_menu" id="fvOu83%(A(u+W4]!u!JS"><field name="BROADCAST_OPTION" id="Q}.j:b^#:g9pcXys:$@_" variabletype="broadcast_msg">message1</field></shadow></value></block>
    val blk = parser.parseBlock(xml)
    println(blk)
    println(blk.toAst().toBlock())
    assert(blk == blk.toAst().toBlock())
    assert(trim(xml) == trim(blk.toAst().toBlock().toXml()))
  }

  test("list-type varialbe as field") {
    val xml = <block y="752" x="203" id="fmsqUj76fmf|,K*EYIW}" type="data_itemoflist"><field variabletype="list" id="H(Vfkw8c{U6fkLwP.8?v" name="LIST">list</field><value name="INDEX"><shadow id="i4.@i`5y;ga;.BH*9T-J" type="math_integer"><field name="NUM">1</field></shadow></value></block>
    val blk = parser.parseBlock(xml)
    assert(trim(blk.toXml) == trim(xml))
    val xml_ordered = blk.toAst().toBlock().toXml()
    println(xml_ordered)
    val blk_ordered = parser.parseBlock(xml_ordered)
    assert(trim(blk_ordered.toAst().toXml()) == trim(xml_ordered))
  }

  test("data variable") {
    val xml = <block type="data_variable" id="AGjp/]{t6wHE,}p@-;k*"><field name="VARIABLE" id="OHx#61F$BHa65~tm_dkn" variabletype="">a</field></block>
    val blk = parser.parseBlock(xml)
    assert(trim(blk.toXml) == trim(xml))
    val xml_ordered = blk.toAst().toBlock().toXml()
    val ast = blk.toAst()
    println(blk.toAst().asInstanceOf[VarAccess].toDevString())
    val blk_ordered = parser.parseBlock(xml_ordered)
    assert(trim(blk_ordered.toAst().toXml()) == trim(xml_ordered))
  }

  test("data_setvar") {
    val xml = <block y="103" x="220" id="}yw!mH3${6_a`!a{AnJ4" type="data_setvariableto" xmlns="http://www.w3.org/1999/xhtml"><field variabletype="" id="E+Fdq5=v`3^n@PBk6I%^" name="VARIABLE">a</field><value name="VALUE"><shadow id="Chd[L|I7|oZ%u-TPPyt-" type="text"><field name="TEXT">0</field></shadow></value></block>
    val blk = parser.parseBlock(xml)
    assert(trim(blk.toXml) == trim(xml))
    val xml_ordered = blk.toAst().toBlock().toXml()
    val blk_ordered = parser.parseBlock(xml_ordered)
    assert(trim(blk_ordered.toAst().toXml()) == trim(xml_ordered))
  }

  test("actual Scratch project") {
    val stream = getClass.getResourceAsStream("/test3.xml")
    val xmlBlocks = scala.io.Source.fromInputStream(stream).getLines.mkString
    val program = parser.parseProgram(xmlBlocks)
    println(program.toDevString)
  }

  test("stop this script") {
    val xml = <block type="control_stop" id="w9cdK,Fvo*/lS|4-U65V"><mutation hasnext="false"></mutation><field name="STOP_OPTION">this script</field></block>
    val blk = parser.parseBlock(xml)
    assert(blk == blk.toAst().toBlock())
    assert(trim(xml) == trim(blk.toAst().toBlock().toXml()))
  }

  test("change var by N") {
    val xml = <block type="data_changevariableby" id="Q8Yx-^C:#kS@s{kG$BO%" x="236" y="189"><field name="VARIABLE" id="1|qo/bO6oF,riM?B41g," variabletype="">a</field><value name="VALUE"><shadow type="math_number" id="0?{srV//=a=*[VdG@]FZ"><field name="NUM">1</field></shadow></value></block>
    val blk = parser.parseBlock(xml)
    assert(blk == blk.toAst().toBlock())
    assert(trim(xml) == trim(blk.toAst().toBlock().toXml()))
  }

  test("procedure with parameter read") {
    val xml = <block type="procedures_definition" id=";@gt+uIE3qmkO1hXP#qC" x="176" y="130"><value name="custom_block"><shadow type="procedures_prototype" id="CcvK%9U^sZnb;]I0kRq7"><mutation proccode="test %s" argumentids="[&quot;M9]ZYkEtx#K*9LbQUXg8&quot;]" argumentnames="[&quot;num&quot;]" argumentdefaults="[&quot;todo&quot;]" warp="false"/><value name="M9]ZYkEtx#K*9LbQUXg8"><shadow type="argument_reporter_string_number" id=",_8[DXagHxPp~Pc`9GXk"><field name="VALUE">num</field></shadow></value></shadow></value><next><block type="motion_movesteps" id="iLGX{^2}];lo._rX-)$t"><value name="STEPS"><shadow type="math_number" id="RN^ek2kkLD5EWK]?VO%_"><field name="NUM">10</field></shadow><block type="argument_reporter_string_number" id="=B}PjC#HU0Af1Nn7Qo^N"><field name="VALUE">num</field></block></value></block></next></block>
    val procdef = parser.parseProcDef(xml)
    println(procdef)
    assert(procdef.toAst.toProcDefEl.toAst.toProcDefEl == procdef.toAst.toProcDefEl)
    assert(trim(procdef.toXml) == trim(xml))
  }

  test("extension blocks") {
    val xml = <block id="%,BSa%LpqUqKW]CFm6.%" type="music_playNoteForBeats"><value name="NOTE"><shadow id="*T~j)#N_F-i.wnhoJru]" type="math_number"><field name="NUM">60</field></shadow></value><value name="BEATS"><shadow id=";4w7djGl%pmjHc}]_GKO" type="math_number"><field name="NUM">0.25</field></shadow></value></block>;
    val blk = parser.parseBlock(xml)
    println(blk.toAst)
  }

  test("block to xml of more than one nested blocks") {
    val xml = <block type="control_forever" id="So:4ZstUKtzP1{^hI=/v" x="115" y="171"><statement name="SUBSTACK"><block type="motion_movesteps" id="S;a.G|jCW[,[e^z{wPMq"><value name="STEPS"><shadow type="math_number" id="W0gN#i:Dqcbx|U(!HiGa"><field name="NUM">10</field></shadow></value><next><block type="motion_turnright" id="aESx@^!Ew__F2094E:FG"><value name="DEGREES"><shadow type="math_number" id=".89-35I?EE$Y$~z!wXs$"><field name="NUM">15</field></shadow></value></block></next></block></statement></block>
    val blk = parser.parseBlock(xml)
    val ast = blk.toAst()
    val blk2 = ast.toBlock()
    assert(trim(blk.toAst().toXml) == trim(xml))
  }

  test("param block use type field in varacc to decide its field") {
    val xml = <block type="procedures_definition" id=";@gt+uIE3qmkO1hXP#qC" x="176" y="130"><value name="custom_block"><shadow type="procedures_prototype" id="CcvK%9U^sZnb;]I0kRq7"><mutation proccode="test %s" argumentids="[&quot;M9]ZYkEtx#K*9LbQUXg8&quot;]" argumentnames="[&quot;num&quot;]" argumentdefaults="[&quot;todo&quot;]" warp="false"></mutation><value name="M9]ZYkEtx#K*9LbQUXg8"><shadow type="argument_reporter_string_number" id=",_8[DXagHxPp~Pc`9GXk"><field name="VALUE">num</field></shadow></value></shadow></value><next><block type="data_setvariableto" id="7aFczy-bgG?5=WQQta?Y"><field name="VARIABLE" id="KVg4SEzjF~1Po[U2e3tf" variabletype="">a</field><value name="VALUE"><shadow type="text" id="XdEJ|sTyjy[*rfG%p!:D"><field name="TEXT">0</field></shadow><block type="argument_reporter_string_number" id="=B}PjC#HU0Af1Nn7Qo^N"><field name="VALUE">num</field></block></value></block></next></block>
    val procdef = parser.parseProcDef(xml)
    println(procdef)
    assert(procdef.toAst.toProcDefEl.toAst.toProcDefEl == procdef.toAst.toProcDefEl)
    assert(trim(procdef.toAst().toProcDefEl.toXml) == trim(xml))
  }
  
  test("target/stage with costumes") {
    val xml = <program><stage name="Stage"><xml><costumes><costume name="backdrop1"/></costumes><variables><variable type="" id="`jEk@4|i[#Fk?(8x)AV.-my variable" islocal="false" iscloud="false">my variable</variable></variables></xml></stage><sprite name="Sprite1"><xml><costumes><costume name="costume1"/><costume name="costume2"/></costumes><variables></variables><block id="[Qu;4$sA@t%HO6ddv9nu" type="motion_movesteps" x="115.70370370370364" y="127.70370370370371" ><value name="STEPS"><shadow id=",sj4lSQ]2VS5j+_w?%_(" type="math_number" ><field name="NUM">10</field></shadow></value></block></xml></sprite></program>
    val program = parser.parseProgram(xml)    
  }

}