package sb3.dsl

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import ast.{ List => ASTList, _ }
import sb3.parser.Ast2ProgramElems._
import scala.xml.Utility.trim
import sb3.parser.xml.Sb3XmlParser

@RunWith(classOf[JUnitRunner])
class DslSpec extends FunSuite with BeforeAndAfter {
  var dslParser: SimXml2AstParser = null

  before {
    dslParser = new SimXml2AstParser()
  }

  test("expr") {
    val xmls = List(
      <expr></expr>,
      <attr>x</attr>,
      <str>a</str>,
      <num>4</num>,
      <expr><expr><num>4</num></expr></expr>)

    for (expr <- xmls) {
      val ast = dslParser.parse(expr)
      println(ast.asInstanceOf[Expr].toDevString())
    }
  }

  test("loop") {
    val xml = <loop type="control_repeat_until"><cond><expr><num>4</num></expr></cond><body></body></loop>
    val ast = dslParser.parse(xml).asInstanceOf[LoopStmt]
    println(ast.toDevString())
  }

  test("statement") {
    val xmls = List(
      <stmt><num>4</num><expr><expr><num>4</num></expr></expr></stmt>,
      <stmt id='1'></stmt>)
    for (stmt <- xmls) {
      val ast = dslParser.parse(stmt)
      println(ast.asInstanceOf[Stmt].toDevString())
    }
  }

  test("ifelse") {
    val ifelsestmt =
      <ifelse>
        <cond><str>true</str></cond>
        <then><stmt><num>4</num></stmt><stmt/></then>
        <else><stmt></stmt></else>
      </ifelse>
    val ast = dslParser.parse(ifelsestmt).asInstanceOf[IfElseStmt]
    println(ast.toDevString())
  }

  test("script") {
    val scriptXml = <script><stmt><num>4</num></stmt></script>
    val ast = dslParser.parse(scriptXml).asInstanceOf[Script]
    println(ast.toDevString())
  }

  test("varacc") {
    val scriptXml = <script><stmt><expr><varacc vid='a'/></expr></stmt></script>
    val ast = dslParser.parse(scriptXml).asInstanceOf[Script]
    println(ast.toDevString())
  }

  test("sprite") {
    val spriteXml = <sprite name="a"><vars><vardecl id='a'/><vardecl id='b'/></vars><script><stmt></stmt></script><script></script></sprite>
    val ast = dslParser.parse(spriteXml).asInstanceOf[Sprite]
    println(ast.toDevString())
  }

  test("assignment") {
    var assignStmt = <assign><dest><varacc vid='a'/></dest><src><num>4</num></src></assign>
    val ast = dslParser.parse(assignStmt).asInstanceOf[AssignStmt]
    println(ast.toDevString())
  }

  test("increment") {
    var incrementStmt = <increment><dest><varacc id='R1' vid='a'/></dest><src><num>1</num></src></increment>
    val ast = dslParser.parse(incrementStmt).asInstanceOf[AssignStmt]
    println(ast.toDevString())
  }

  test("stop this script") {
    var stopStmt = <stop id='stop1'/>
    val ast = dslParser.parse(stopStmt).asInstanceOf[StopStmt]
    println(ast.toDevString())
  }

  test("procedure") {
    var procedure = <procedure proccode="M"><params><param id='p1'/></params><body><stmt id='s1'></stmt></body></procedure>
    val ast = dslParser.parse(procedure).asInstanceOf[ProcDecl]
    println(ast.toDevString())
  }

  test("procedure call") {
    var call = <call proccode="M"><args><arg id='p1'><num>4</num></arg></args></call>
    val ast = dslParser.parse(call).asInstanceOf[ProcAccess]
    println(ast.toDevString())
  }

  test("broadcast") {
    var broadcast = <broadcast wait='true'><str>a</str></broadcast>
    val ast = dslParser.parse(broadcast).asInstanceOf[BroadcastStmt]
    println(ast.toDevString())
  }
  
  test("receive"){
    var receive = <receive msg='a'/>
    val ast = dslParser.parse(receive).asInstanceOf[ReceiveStmt]
    println(ast.toDevString())
  }

  test("program") {
    var program = <program>
                    <vars><vardecl id='g1'/></vars>
										<stage><script><stmt id='1'></stmt></script></stage>
                    <sprite name='s1'><script><stmt id='2'><num>1</num></stmt><stmt id='3'></stmt></script></sprite>
                    <sprite name='s2'><script></script></sprite>
                  </program>
    val ast = dslParser.parse(program).asInstanceOf[Program]
    println(ast.toDevString())
  }

}