package sb3.parser
import play.api.libs.json._

object ParsingUtil {
   def objListToJsonStr(lst:List[Any]): String = {
    val jsVals:List[Any] = lst.map(v => v match {
      case v:String => new JsString(v)
      case v:Integer => new JsNumber(new java.math.BigDecimal(v))
      case _ =>
    })
    var arr = new JsArray
    for(v <- jsVals) arr = arr:+v.asInstanceOf[JsValue]
    arr.toString()
  }
  
  def extractListOfString(strList: String): List[String] = {
    Json.parse(strList.replaceAll("&quot;", "\"")).as[JsArray].value.map(_.as[String]).toList
  }
  
  def extractObjList(jsonList: String): List[Object] = {
    Json.parse(jsonList.replaceAll("&quot;", "\"")).as[JsArray].value.map( v => v match {
      case v:JsString => v.as[String].asInstanceOf[Object]
      case v:JsNumber => v.value.bigDecimal.toBigInteger.intValue.asInstanceOf[Object]
      case _ => v.toString().asInstanceOf[Object]
    }).toList
  }
  
   import scala.util.Random
   def getRandomId():String = Random.alphanumeric.take(6).mkString
   def getRandomId(prefix:String):String = prefix+getRandomId()
}