package sb3.parser.xml

import scala.xml._
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import sb3.parser.Ast2ProgramElems._
import scala.xml.Utility.trim
import sb3.parser._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.LinkedHashMap
import ast.{ List => ASTList, _ }

/*
 * parse to intermediate representation as Scala case classes
 */
class Sb3XmlParser {
  def stringToXmlElem(str: String): Node = scala.xml.XML.loadString(str)
  def parseBlock(xml: Node): BlockEl = {
    trim(xml) match {
      case b @ <block></block> => {
        val opcode = xml.attribute("type").map(_.toString)
        if (!opcode.isDefined) throw new Exception("Undefined block type:" + b)
        if (!Sb3Spec.getSpec(opcode.get).isDefined) Sb3Spec.inferSpec(opcode.get, List())
        BlockEl(b.attributes.asAttrMap, List(), None)
      }
      case b @ <block>{ children @ _* }</block> => {

        var args: List[Arg] = children.map(child => {
          child.label match {
            case "value" if child.attribute("name").map(_.toString).contains("SUBSTACK2") => parseStatement(child) //TODO: merge the two SUBSTACK cases
            case "value" if child.attribute("name").map(_.toString).contains("SUBSTACK") => parseStatement(child)
            case "value" => parseValue(child)
            case "field" => parseField(child)
            case "statement" => parseStatement(child)
            case "mutation" => parseMutation(child)
            case "next" => null
            case _ => throw new Exception("Unknown child elements in block ")
          }
        }).toList.filter(Option(_).isDefined)

        val nextBlock = if (!(xml \ "next").isEmpty) Some((xml \ "next" \ "block")(0)).map(parseBlock(_)) else None

        val opcode = xml.attribute("type").map(_.toString)

        val spec = Sb3Spec.getSpec(opcode.getOrElse("")).orElse(Sb3Spec.inferSpec(opcode.get, args))
        var argKV = LinkedHashMap[String, Option[Arg]]()

        // ordering according to spec spec.get.argMap.map(argSpec => argKV(argSpec.name) = None)
        // not necessary in the same order of the xml input

        for (i <- 0 until args.size) {
          val argName = args(i).attr.getOrElse("name", "__" + i.toString())
          argKV(argName) = Option(args(i))
        }

        val blk = BlockEl(b.attributes.asAttrMap, argKV.values.toList, nextBlock)
        blk
      }

      case other => println(other); throw new Exception("Malformed block")
    }
  }

  def parseValue(xml: Node): Value = {
    trim(xml) match {
      case v @ <value>{ children @ _* }</value> => {
        var someShad: Option[Shadow] = None
        var someBlock: Option[BlockEl] = None
        for (child <- children) child.label match {
          case "shadow" => someShad = Some(parseShadow(child))
          case "block"  => someBlock = Some(parseBlock(child))
          case _        => throw new Exception("Unknown element in value")
        }
        Value(v.attributes.asAttrMap, someShad, someBlock)
      }
      case _ => throw new Exception("Malformed value")
    }

  }

  def parseShadow(xml: Node): Shadow = {
    trim(xml) match {
      case s @ <shadow>{ children @ _* }</shadow> => {
        var args = children.map(child => {
          child.label match {
            case "field"    => parseField(child)
            case "value"    => parseValue(child)
            case "mutation" => parseMutation(child)
          }
        })
        Shadow(s.attributes.asAttrMap, args.toList)
      }
      case s @ <shadow>{ fieldXml @ <field>{ _* }</field> }</shadow> => Shadow(s.attributes.asAttrMap, List(parseField(fieldXml)))
      case s @ <shadow>{ mutationXml @ <mutation>{ _* }</mutation> }</shadow> => Shadow(s.attributes.asAttrMap, List(parseMutation(mutationXml)))
      case _ => throw new Exception("Malformed shadow")
    }
  }

  def parseField(xml: Node): Field = {
    trim(xml) match {
      case f @ <field>{ value }</field> => Field(f.attributes.asAttrMap, value.text)
      case f @ <field></field>          => Field(f.attributes.asAttrMap, "")
      case _                            => throw new Exception("Malformed field")
    }
  }

  def parseMutation(xml: Node): Mutation = {
    trim(xml) match {
      case f @ <mutation></mutation> => Mutation(f.attributes.asAttrMap)
      case _                         => throw new Exception("Malformed field")
    }
  }

  def parseStatement(xml: Node): StatementEl = {
    trim(xml) match {
      case v @ <value>{ topblock }</value>         => StatementEl(v.attributes.asAttrMap, block = Option(parseBlock(topblock)))
      case s @ <statement>{ topblock }</statement> => StatementEl(s.attributes.asAttrMap, block = Option(parseBlock(topblock)))
      case _                                       => throw new Exception("Malformed statement")
    }
  }

  def parseProcDefSignature(xml: Node): Value = {
    trim(xml) match {
      case v @ <value>{ shadow }</value>         => Value(v.attributes.asAttrMap, shadow = Some(parseShadow(shadow)))
      case s @ <statement>{ shadow }</statement> => Value(s.attributes.asAttrMap, shadow = Some(parseShadow(shadow)))
      case _                                     => throw new Exception("Malformed statement")
    }
  }

  def parseCostume(xml: Node): CostumeEl = xml match {
    case costumeNode @ <costume/> => CostumeEl(name= costumeNode.attribute("name").map(_.toString).get)
    case _ => throw new Exception("Unknown costume element: " + xml.toString())
  }
  
  def parseCostumes(xml: Node): List[CostumeEl] = xml match {
    case <costumes>{ costumes @ _* }</costumes> => costumes.filter(_ != new Text(",")).map(parseCostume(_)).toList
    case <costumes/> => List()
    case _ => throw new Exception("Unknown costumes element: " + xml.toString)
  }
  
  def parseSound(xml: Node): SoundEl = xml match {
    case soundNode @ <sound/> => SoundEl(name= soundNode.attribute("name").map(_.toString).get)
    case _ => throw new Exception("Unknown sound element: " + xml.toString())
  }
  
  def parseSounds(xml: Node): List[SoundEl] = xml match {
    case <sounds>{ sounds @ _* }</sounds> => sounds.filter(_ != new Text(",")).map(parseSound(_)).toList
    case <sounds/> => List()
    case _ => throw new Exception("Unknown sounds element: " + xml.toString)
  }
  
  def parseVar(xml: Node): VariableEl = xml match {
    case varNode @ <variable>{ name }</variable> => {
      VariableEl(id = varNode.attribute("id").map(_.toString).get, name = name.text)
    }
    case _ => throw new Exception("Unknown var element: " + xml.toString())
  }

  def parseVars(xml: Node): List[VariableEl] = xml match {
    case <variables>{ vardecls @ _* }</variables> => vardecls.filter(_ != new Text(",")).map(parseVar(_)).toList
    case <variables></variables>                  => List()
    case _                                        => throw new Exception("Unknown variables element: " + xml.toString())
  }

  def parseProcDef(xml: Node): ProcDefEl = xml match {
    case topBlock @ <block>{ children @ _* }</block> => {
      val signatureXmlOpt = children.collectFirst { case child if (child.label == "value" || child.label == "statement") => child }
      val sigValue = parseProcDefSignature(signatureXmlOpt.getOrElse(throw new Exception("ProcDef does not have signature info")))
      val next = children.collectFirst { case child if child.label == "next" => child }
      val body: Option[BlockSeqEl] = if (next.isDefined) Some(BlockSeqEl(parseBlock((next.get \ "block")(0)))) else None
      ProcDefEl(topBlock.attributes.asAttrMap, sigValue, body)
    }
  }

  def parseTarget(xml: Node, name: String = "", attr: Map[String, String]= Map()): SpriteEl = {
    var costumes = new ListBuffer[CostumeEl]()
    var sounds = new ListBuffer[SoundEl]()
    var vars = new ListBuffer[VariableEl]()
    var scripts = new ListBuffer[ScriptEl]()
    var procDefs = new ListBuffer[ProcDefEl]()
    trim(xml) match {
      case <xml>{ children @ _* }</xml> => {
        children.filter(child => child.label == "costumes")
          .collectFirst { case costumesXml => parseCostumes(costumesXml).foreach(c => costumes += c) }
        
        children.filter(child => child.label == "sounds")
          .collectFirst { case soundsXml => parseSounds(soundsXml).foreach(s => sounds += s) }
        
        children.filter(child => child.label == "variables")
          .collectFirst { case variables => parseVars(variables).foreach(v => vars += v) }

        children.filter(child => child.label == "block" && child.attribute("type").get.toString == "procedures_definition")
          .collect { case topBlockXml => procDefs += parseProcDef(topBlockXml) }

        children.filter(child => child.label == "block" && child.attribute("type").get.toString != "procedures_definition")
          .collect { case topBlock => scripts += ScriptEl(BlockSeqEl(parseBlock(topBlock))) }
      }
    }
    SpriteEl(name = name, attr=attr, costumes= costumes.toList, sounds= sounds.toList, vars = vars.toList, procDefs = procDefs.toList, scripts = scripts.toList)
  }

  def parseStage(xml: Node, sprites: List[SpriteEl]): StageEl = {
    var costumes = new ListBuffer[CostumeEl]() //call backdrop
    var sounds = new ListBuffer[SoundEl]()
    var vars = new ListBuffer[VariableEl]()
    var scripts = new ListBuffer[ScriptEl]()
    var procDefs = new ListBuffer[ProcDefEl]()
    trim(xml) match {
      case <xml>{ children @ _* }</xml> => {
        children.filter(child => child.label == "costumes")
          .collectFirst { case costumesXml => parseCostumes(costumesXml).foreach(c => costumes += c) }
        children.filter(child => child.label == "sounds")
          .collectFirst { case soundsXml => parseSounds(soundsXml).foreach(s => sounds += s) }
        children.filter(child => child.label == "variables")
          .collectFirst { case variables => parseVars(variables).foreach(v => vars += v) }
        children.filter(child => child.label == "block" && child.attribute("type").get.toString == "procedures_definition")
          .collect { case topBlockXml => procDefs += parseProcDef(topBlockXml) }
        children.filter(child => child.label == "block" && child.attribute("type").get.toString != "procedures_definition")
          .collect { case topBlock => scripts += ScriptEl(BlockSeqEl(parseBlock(topBlock))) }
      }
    }
    StageEl(costumes = costumes.toList, sounds= sounds.toList, vars = vars.toList, scripts = scripts.toList, procDefs = procDefs.toList, sprites = sprites)
  }

  def parseProgram(xml: Node): ProgramEl = {
    var sprites = new ListBuffer[SpriteEl]()
    val stageEl = trim(xml) match {
      case <program>{ targets @ _* }</program> => {
        targets.filter { tar => tar.label == "sprite" }
          .collect {
            case spriteNode @ <sprite>{ xml }</sprite> => {
              try{
                sprites += parseTarget(xml, spriteNode.attribute("name").map(_.toString).get, spriteNode.attributes.asAttrMap)
              }catch{
                case e:Exception => println("parsing failure @"+spriteNode.attribute("name")) 
              }
            }
          }
        targets.filter(tar => tar.label == "stage").collectFirst {
          case <stage>{ xml }</stage> => parseStage(xml, sprites.toList)
        }
      }
    }
    ProgramEl(stageEl.get)
  }

  import sb3.parser.Ast2ProgramElems._

  def parseProgram(xmlStr: String): Program = {
    val programEl = parseProgram(stringToXmlElem(xmlStr))
    programEl.toAst()
  }

  def parseTarget(xmlStr: String): Sprite = {
    val spriteEl = parseTarget(stringToXmlElem(xmlStr))
    spriteEl.toAst()
  }

  def parseBlock(xmlStr: String): ScratchBlock = {
    val blockEl = parseBlock(stringToXmlElem(xmlStr))
    blockEl.toAst()
  }
  
  def parseBlockSeq(xmlStr: String): BlockSeq = {
    parseBlock(stringToXmlElem(xmlStr)).toBlockSeq();
  }
}

