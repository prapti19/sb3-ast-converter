package sb3.parser.json
import play.api.libs.json._
import play.api.libs.functional.syntax._
import sb3.parser.Field

case class Input(name: String, shadowVal: String, shadowBlock: String, blockId: Option[String])
object ExtractorHelper {
  def extractInputList(inputsJson: JsObject, blockMap: JsObject): List[Input] = {
    val inputKVs: JsObject = (inputsJson \ "inputs").get.asInstanceOf[JsObject]
    inputKVs.fields.foldLeft(List.empty[Input])((lst, entry) =>
      Input(
        entry._1,
        extractShadowInputStr(entry._2.asInstanceOf[JsArray], blockMap),
        extractShadowBlockInputStr(entry._2.asInstanceOf[JsArray], blockMap),
        extractBlockInputIDStr(entry._2.asInstanceOf[JsArray], blockMap)) :: lst)
  }
  
  def extractFieldList(inputsJson: JsObject, blockMap: JsObject): List[Option[Field]] = {
    val inputKVs: JsObject = (inputsJson \ "fields").get.asInstanceOf[JsObject]
    inputKVs.fields.foldLeft(List.empty[Option[Field]])((lst, entry) =>  {
      var attr = collection.mutable.Map[String, String]()
      if(entry._2.as[JsArray].value.size == 2){
        attr += ("name" -> entry._1)
        attr += ("id" -> entry._2.apply(1).asOpt[String].getOrElse(entry._2.apply(0).toString()))
      } else {
        attr += ("name" -> entry._1)
      }
      Option(Field(attr.toMap, entry._2.apply(0).asOpt[String].getOrElse(entry._2.apply(0).toString()))) :: lst})
  }

  // shadow argument
  def extractShadowInputStr(jsArr: JsArray, blockMap: JsObject): String = {
    val buff = (jsArr.value.filter(p => p.isInstanceOf[JsArray]).collect {
      case a: JsArray => {
        var shadow = a.value(1).asOpt[String].getOrElse(a.value(1).toString()) //needs to be done otherwise error thrown
        if (shadow == "") {
          shadow = "0" //to add default shadow value
        }
        shadow
      }
      case _ => null
    })
    if (buff.isEmpty) {
      ""
    } else {
      buff.filter(_ != null)(0)
    }
  }

  // shadow block argument
  def extractShadowBlockInputStr(jsArr: JsArray, blockMap: JsObject): String = {
    val buff = (jsArr.value.filter(p => p.isInstanceOf[JsArray]).collect {
      case a: JsArray => {
        val shadow = a.value(1).asOpt[String].getOrElse(a.value(1).toString()) //needs to be done otherwise error thrown
        shadow
      }
      case _ => null
    })
    if (buff.isEmpty) {
      val shadowBlockId = jsArr.value.apply(1).asOpt[String].getOrElse(jsArr.value.apply(1).toString())
      if (shadowBlockId.equals("null")) {
        ""
      } else {
        val shadowBlock = (blockMap \ shadowBlockId)
        val isShadow = (shadowBlock \ "shadow").as[Boolean]
        //to add shadow blocks only
        if (isShadow) {
          shadowBlockId
        } else {
          ""
        }
      }
    } else {
      ""
    }
  }

  // block input argument is optional
  def extractBlockInputIDStr(jsArr: JsArray, blockMap: JsObject): Option[String] = {
    val buff = (jsArr.value.filter(p => p.isInstanceOf[JsString]).collect {
      case idJsStr: JsString => Some(idJsStr.as[String])
      case _ => None
    })
    if (buff.isEmpty) {
      None
    } else {
      val blockId = buff(0).toString().substring(5, 25)
      val shadowBlock = (blockMap \ blockId)
      val isShadow = (shadowBlock \ "shadow").as[Boolean]
      //to add blocks which are not shadow
      if (isShadow) {
        None
      } else {
        buff(0)
      }
    }
  }
}