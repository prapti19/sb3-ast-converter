package sb3.parser
import sb3.data.{ Default }
import scala.xml._
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._

import scala.collection.mutable.ListBuffer
import sb3.parser.Ast2ProgramElems._
import ast.{ List => ASTList, _ }
import play.api.libs.json._

/*
 *  an object containing implicit definition (wrappers of ast node)
 *  to enhance existing Ast node with functionalities to convert itself to
 *  its corresponding program elements e.g.
 *  ast node classes which implements ScratchBlock interface toBlock(),
 *  default.toShadow(), etc
 *  and toXml()
 */
object Ast2ProgramElems {
  implicit def opExpr2Block(astNode: OperatorExpr) = new OpExprConvert(astNode)
  implicit def default2Shadow(default: Default) = new DefaultConvert(default)
  implicit def stmt2Block(astNode: ExprStmt) = new ExprStmtConvert(astNode)
  implicit def stmt2Block(astNode: LoopStmt) = new LoopStmtConvert(astNode)
  implicit def stmt2Block(astNode: IfElseStmt) = new IfElseStmtConvert(astNode)
  implicit def scratchBlockAst2Block(astNode: ScratchBlock) = new ScratchBlockAstConvert(astNode)
  implicit def blockSeq2ConnectedBlock(astNode: BlockSeq) = new BlockSeqConvert(astNode)
  implicit def script2Elem(astNode: Script) = new ScriptConvert(astNode)
  implicit def costumeConvert(astNode: Costume) = new CostumeConvert(astNode)
  implicit def varDeclConvert(astNode: VarDecl) = new VarDeclConvert(astNode)
  implicit def spriteConvert(astNode: Sprite) = new SpriteConvert(astNode)
  implicit def stageConvert(astNode: Stage) = new StageConvert(astNode)
  implicit def programConvert(astNode: Program) = new ProgramConvert(astNode)
  implicit def procDeclConvert(astNode: ProcDecl) = new ProcDeclConvert(astNode)
  implicit def procAccConvert(astNode: ProcAccess) = new ProcAccConvert(astNode)
  implicit def receiveStmtConvert(astNode: ReceiveStmt) = new ReceiveStmtConvert(astNode)
  implicit def broadcastStmtConvert(astNode: BroadcastStmt) = new BroadcastStmtConvert(astNode)
  implicit def varaccessConvert(astNode: VarAccess) = new VarAccessConvert(astNode)
  implicit def stopStmtConvert(astNode: StopStmt) = new StopStmtConvert(astNode)
  implicit def assignStmtConvert(astNode: AssignStmt) = new AssignStmtConvert(astNode)

  def exprToArgType(astNode: Option[Any], default: Option[Default], name: Option[String]) = {
    val attr: Map[String, String] = if (name.isDefined) Map("name" -> name.get) else Map()
    astNode match {
      case Some(arg: OperatorExpr) => {
        val someShad = if (default == null) None else default.map(_.toShadow())
        Option(Value(attr, someShad, Some(arg.toBlock)))
      }
      case Some(arg: Literal)    => Option(Value(attr, default.map(_.toShadow()), None))
      case Some(arg: AttrAccess) => Option(Field(attr, arg.getID)) //attribute id = name
      case Some(arg: VarAccess)  => {
        
        if(arg.getType=="broadcast_msg"){
          Option(Value(attr,default.map(_.toShadow()),block=None))
        }else if(arg.getType=="list"){
          Option(arg.toField()) //when varacc is refered as field in data block
        }else if(arg.getType==""){
          val valOpt = Option(Value(attr,default.map(_.toShadow()),block=Some(arg.toBlock)))
          valOpt
        }
        else{ //when varacc (non msg var) acts as varexp //todo msgvar type? to make it less confusing
          Option(Value(attr,default.map(_.toShadow()),block=Some(arg.toBlock)))
        }
      }
      case Some(arg: BlockSeq) => {
        val firstBlock = if (arg.getNumStmt > 0) Option(arg.toBlockSeqEl.topBlock) else None
        if (firstBlock.isDefined) Option(StatementEl(attr, firstBlock)) else None
      }
      case None => None
      case _    => throw new Exception("Unmatched arg type: " + astNode)
    }
  }
}

class OpExprConvert(node: OperatorExpr) {
  def toBlock(): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    val spec = Sb3Spec.getSpec(node.getOpcode)
    for (i <- 0 until spec.get.argMap.size) {
      args += exprToArgType(Option(node.getArg(i)), if (node.getDefault(i) != null) Option(node.getDefault(i)) else None, Sb3Spec.getArgName(node.getOpcode, i))
    }

    BlockEl(node.getAttrs.asScala.toMap, args.toList, None)
  }

  def toXml() = toBlock().toXml()
}

import scala.xml.Utility.trim

class ScratchBlockAstConvert(node: ScratchBlock) {
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
    node match {
      case node: StopStmt      => node.toBlock(next)  //subtype of ExprStmt needs to match first
      case node: ExprStmt      => node.toBlock(next)
      case node: LoopStmt      => node.toBlock(next)
      case node: IfElseStmt    => node.toBlock(next)
      case node: OperatorExpr  => node.toBlock()
      case node: VarAccess     => node.toBlock()
      case node: ProcAccess    => node.toBlock(next)
      case node: ReceiveStmt   => node.toBlock(next)
      case node: BroadcastStmt => node.toBlock(next)
      case node: AssignStmt    => node.toBlock(next)
      case _                   => print(node); throw new Exception(node+":unhandled ast node type for block conversion")
    }
  }
  def toXml(): scala.xml.Node = trim(toBlock().toXml())
}

class ExprStmtConvert(node: ExprStmt) {
  def toBlock(): BlockEl = toBlock(None)

  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    for (i <- 0 until node.getNumArg) {
      args += exprToArgType(Option(node.getArg(i)), Option(node.getDefault(i)), Sb3Spec.getArgName(node.getOpcode, i))
    }

    BlockEl(node.getAttrs.asScala.toMap, args.toList, next)
  }

  def toXml() = toBlock().toXml()
}

class StopStmtConvert(node: StopStmt) {
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    args += Some(Mutation(attr = Map("hasnext" -> "false")))
    for (i <- 0 until node.getNumArg) {
      args += exprToArgType(Option(node.getArg(i)), Option(node.getDefault(i)), Sb3Spec.getArgName(node.getOpcode, i))
    }
    BlockEl(node.getAttrs.asScala.toMap, args.toList, next)
  }
  def toXml() = toBlock().toXml()
}

class ReceiveStmtConvert(node: ReceiveStmt) {
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()

    args += Option(Field(attr = node.getDefault(0).getFieldAttr.asScala.toMap, value = node.getMsg().getName))
    BlockEl(node.asInstanceOf[ScratchBlock].getAttrs.asScala.toMap, args.toList, next)
  }
}

class BroadcastStmtConvert(node: BroadcastStmt) {
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    args += exprToArgType(Option(node.getMsg()), Option(node.getDefault(0)), Sb3Spec.getArgName(node.getOpcode, 0))
    BlockEl(node.asInstanceOf[ScratchBlock].getAttrs.asScala.toMap, args.toList, next)
  }
}

class VarAccessConvert(node: VarAccess) {
  def toField(): Field = node.getType match {
    case "" => Field(attr = Map("name" -> "VARIABLE", "id" -> node.getID, "variabletype" -> ""), value = node.getName)
    case "list" => Field(attr = Map("name" -> "LIST", "id" -> node.getID, "variabletype" -> "list"), value = node.getName)
    case "broadcast_msg" => Field(attr = Map("name" -> "BROADCAST_OPTION", "id" -> node.getID, "variabletype" -> "broadcast_msg"), value = node.getName)
    case "argument_reporter_string_number" => Field(attr=Map("name"->"VALUE"), value=node.getName)  //ParamAccess
    case "argument_reporter_boolean" => Field(attr=Map("name"->"VALUE"), value=node.getName)  //ParamAccess
    case _ => throw new Exception("Unknown variabletype")
  }
  
  // when varaccess is Expr w/ opcode=data_variable 
  // similar to operator expr
  def toBlock(): BlockEl = {
    BlockEl(node.getAttrs.asScala.toMap, List(Some(toField())), None)
  }
}

class AssignStmtConvert(node: AssignStmt){
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
		 var args = new ListBuffer[Option[Arg]]()
    args += Option(node.getDest().toField())
    args += exprToArgType(Option(node.getSource()), Option(node.getDefault(1)), Sb3Spec.getArgName(node.getOpcode, 1))
    BlockEl(node.asInstanceOf[ScratchBlock].getAttrs.asScala.toMap, args.toList, next)
  }
}

class ProcAccConvert(node: ProcAccess) {
  def toBlock(): BlockEl = toBlock(None)
  def toBlock(next: Option[BlockEl]): BlockEl = {
    var argList = new ListBuffer[Option[Arg]]()
    argList += Option(Mutation(Map("proccode" -> node.getMeta.getProccode, "argumentids" -> ParsingUtil.objListToJsonStr(node.getMeta.getArgumentIds.toList), "warp" -> "null")))
    for (i <- 0 until node.getNumArg) {
      argList += exprToArgType(Option(node.getArg(i)), Option(node.getDefault(i)), Option(node.getMeta.getArgumentIds.get(i)))
    }
    BlockEl(node.getAttrs.asScala.toMap, argList.toList, next)
  }
}

class LoopStmtConvert(node: LoopStmt) {
  def toBlock(): BlockEl = toBlock(None)

  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    node.getOpcode match {
      case "control_forever" => args += exprToArgType(Option(node.getBody), null, Sb3Spec.getArgName(node.getOpcode, 0))
      case otherType => {
        args += exprToArgType(Option(node.getCond), Option(node.getDefault(0)), Sb3Spec.getArgName(node.getOpcode, 0))
        args += exprToArgType(Option(node.getBody), Option(null), Sb3Spec.getArgName(node.getOpcode, 1))
      }
    }

    BlockEl(node.getAttrs.asScala.toMap, args.filter { _.isDefined }.toList, next)
  }

  def toXml() = toBlock().toXml()
}

class ProcDeclConvert(node: ProcDecl) {

  def toProcDefEl(): ProcDefEl = {
    var argList: ListBuffer[Arg] = new ListBuffer[Arg]()
    argList += Mutation(Map(
      "proccode" -> node.getMeta.getProcCode,
      "argumentids" -> ParsingUtil.objListToJsonStr(node.getMeta.getArgIds.toList),
      "argumentnames" -> ParsingUtil.objListToJsonStr(node.getMeta.getArgNames.toList),
      "argumentdefaults" -> ParsingUtil.objListToJsonStr(node.getMeta.getArgDefaults.toList),
      "warp" -> node.getMeta.isWarp.toString()))

    node.getParamDeclList.asScala.foreach { p =>
      argList += Value(
        attr = Map("name" -> p.getMeta.getName),
        shadow = Some(Shadow(
          attr = Map("id" -> p.getMeta.getShadowId, "type" -> p.getMeta.getShadowType),
          args = List(Field(attr = Map("name" -> p.getMeta.getFieldName), value = p.getMeta.getFieldValue)))))
    }
    val value = Value(
      attr = Map("name" -> "custom_block"),
      shadow = Some(Shadow(
        attr = Map("id" -> node.getMeta.getShadowId, "type" -> node.getMeta.getShadowType),
        args = argList.toList)))

    ProcDefEl(node.getAttrs.asScala.toMap, value, if (node.getBody.hasStmt()) Some(node.getBody.toBlockSeqEl) else None)
  }

}

class IfElseStmtConvert(node: IfElseStmt) {
  def toBlock(): BlockEl = toBlock(None)

  def toBlock(next: Option[BlockEl]): BlockEl = {
    var args = new ListBuffer[Option[Arg]]()
    args += exprToArgType(Option(node.getCond), Option(node.getDefault(0)), Sb3Spec.getArgName(node.getOpcode, 0))
    args += exprToArgType(Option(node.getThen), None, Some("SUBSTACK"))
    args += exprToArgType(Option(node.getElse), None, Some("SUBSTACK2"))

    BlockEl(node.getAttrs.asScala.toMap, args.toList, next)
  }

  def toXml() = toBlock().toXml()
}

class BlockSeqConvert(node: BlockSeq) {

  def blockItr(itr: Iterator[Stmt]): BlockEl = itr.foldRight(null: BlockEl) {
    (stmt, next) => stmt.asInstanceOf[ScratchBlock].toBlock(Option(next))
  }

  def toBlock(): BlockEl = {
    blockItr(node.getStmtList.iterator.asScala)
  }

  def toBlockSeqEl(): BlockSeqEl = {
    BlockSeqEl(toBlock())
  }

  def toXml() = node.toBlock().toBlockSeqXml()
}

class ScriptConvert(script: Script) {
  def toScriptEl(): ScriptEl = {
    ScriptEl(script.getBody.toBlockSeqEl())
  }
  def toXml() = script.toScriptEl().toXml()
}

class CostumeConvert(costume: Costume) {
  def toCostumeEl(): CostumeEl = CostumeEl(name = costume.getName)
}

class VarDeclConvert(varDecl: VarDecl) {
  def toVarDecl(): VariableEl = VariableEl(varType = varDecl.getVarType, id = varDecl.getID, name = varDecl.getName)
}

class SpriteConvert(sprite: Sprite) {
  def toSpriteEl(): SpriteEl = {
    val costumeEls = sprite.getCostumeList.asScala.collect { case costume => costume.toCostumeEl() }
    val soundEls = sprite.getSounds.asScala.collect { case sound => sound }
    val varEls = sprite.getVarDeclList.asScala.collect { case varDecl => varDecl.toVarDecl() }
    val procDefEls = sprite.getProcDeclList.asScala.collect { case procDecl => procDecl.toProcDefEl() }
    val scriptEls = sprite.getScriptList.asScala.collect { case script => script.toScriptEl() }
    SpriteEl(name = sprite.getName, attr = sprite.getAttrs.asScala.toMap, vars = varEls.toList, procDefs = procDefEls.toList, scripts = scriptEls.toList)
  }
  def toXml() = sprite.toSpriteEl().toXml()
}

class StageConvert(stage: Stage) {
  def toStageEl(): StageEl = {
    val varEls = stage.getVarDeclList.asScala.collect { case varDecl => varDecl.toVarDecl() }
    val scriptEls = stage.getScriptList.asScala.collect { case script => script.toScriptEl() }
    val procDefEls = stage.getProcDeclList.asScala.collect { case procDecl => procDecl.toProcDefEl() }
    val spriteEls = stage.getSpriteList.asScala.collect { case sprite => sprite.toSpriteEl() }
    StageEl(vars=varEls.toList, scripts=scriptEls.toList, procDefs=procDefEls.toList, sprites=spriteEls.toList)
  }
  def toXml() = stage.toStageEl().toXml()
}

class ProgramConvert(program: Program) {
  def toProgramEl(): ProgramEl = {
    val stageEl = program.getStage.toStageEl()
    ProgramEl(stageEl)
  }
  def toXml() = program.toProgramEl().toXml()
}

class DefaultConvert(default: Default) {
  def toShadow(): Shadow = {
    val field = Field(default.getFieldAttr.asScala.toMap, default.getValue)
    Shadow(default.getShadowAttr.asScala.toMap, List(field))
  }

  def toXml() = toShadow().toXml()
}






